<div align="center">
<img width="258" src="images/header.png" alt="minimal gui logo" />
<h4>Single-header library for DLL injection via thread hijacking.</h4>

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-c-plus-plus.svg)](https://forthebadge.com)
</div>

# 

Single-header C++ library that provides functionality for DLL injection via thread hijacking, targeting specific processes identified by window titles.

### Example
```cpp
#include "DLLInjector.hpp"
#include <iostream>

int main() {
    try {
        const char* dll_name = "ExampleDLL.dll";
        const char* target_window_title = "Target Application";

        std::cout << "Attempting to inject DLL...\n";

        int result = injector::inject_dll(dll_name, target_window_title);

        if (result == 0) {
            std::cout << "DLL was successfully injected.\n";
        } else {
            std::cout << "Failed to inject DLL.\n";
        }
    } catch (const std::exception& e) {
        std::cerr << "Exception occurred: " << e.what() << '\n';
    }

    return 0;
}
```

</div>