/**
 * @file dll_injector.hpp
 * @brief Single-header library for DLL injection via thread hijacking.
 * @author Max McNeal
 */

#ifndef DLL_INJECTOR_HPP
#define DLL_INJECTOR_HPP

#include <windows.h>
#include <tlhelp32.h>
#include <string>
#include <cstring>
#include <sys/stat.h>

// Define privileges and other constants
constexpr ULONG SE_DEBUG_PRIVILEGE = 20;
constexpr size_t SHELL_CODE_SIZE = 37;

// Declaration for external NTAPI function
extern "C" NTSTATUS NTAPI RtlAdjustPrivilege(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);

namespace injector {

/**
 * Shell code to be injected.
 */
constexpr unsigned char SHELL_CODE[] = {
    0x60, 0xE8, 0x00, 0x00, 0x00, 0x00, 0x5B, 0x81, 0xEB, 0x06, 0x00, 0x00,
    0x00, 0xB8, 0xCC, 0xCC, 0xCC, 0xCC, 0x8D, 0x93, 0x22, 0x00, 0x00, 0x00,
    0x52, 0xFF, 0xD0, 0x61, 0x68, 0xCC, 0xCC, 0xCC, 0xCC, 0xC3
};

/**
 * Check if a file exists.
 * @param file_name Name of the file to check.
 * @return True if the file exists, false otherwise.
 */
bool file_exists(const std::string& file_name) {
    struct stat buffer;
    return (stat(file_name.c_str(), &buffer) == 0);
}

/**
 * Display an error message and exit.
 * @param error_title Title for the error dialog box.
 * @param error_message Message to be displayed.
 */
void error(const char* error_title, const char* error_message) {
    MessageBox(NULL, error_message, error_title, NULL);
    std::exit(-1);
}

/**
 * Retrieve the process ID associated with a window title.
 * @param window_title The title of the window.
 * @param process_id Reference to store the process ID.
 */
void get_process_id(const char* window_title, DWORD& process_id) {
    GetWindowThreadProcessId(FindWindow(NULL, window_title), &process_id);
}

/**
 * Main function to perform DLL injection via thread hijacking.
 * @param dll_name Name of the DLL to inject.
 * @param window_title Window title of the target process.
 * @return Status code; 0 on success.
 */
int inject_dll(const char* dll_name, const char* window_title) {
    DWORD process_id = 0;
    BOOLEAN privilege_enabled = FALSE;
    HANDLE h_process, h_thread, h_snapshot;
    PVOID allocated_memory, buffer;
    THREADENTRY32 te32;
    CONTEXT ctx;

    char dll_path[MAX_PATH];
    RtlAdjustPrivilege(SE_DEBUG_PRIVILEGE, TRUE, FALSE, &privilege_enabled);

    if (!file_exists(dll_name)) {
        error("File Not Found", "The specified DLL file does not exist.");
    }

    if (!GetFullPathName(dll_name, MAX_PATH, dll_path, nullptr)) {
        error("Path Error", "Failed to resolve the full path of the DLL.");
    }

    get_process_id(window_title, process_id);
    if (process_id == 0) {
        error("Process Error", "Failed to obtain the process ID.");
    }

    h_process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, process_id);
    if (!h_process) {
        error("OpenProcess Failed", "Unable to obtain a handle to the target process.");
    }

    h_snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    te32.dwSize = sizeof(THREADENTRY32);

    if (Thread32First(h_snapshot, &te32)) {
        do {
            if (te32.th32OwnerProcessID == process_id) {
                break;
            }
        } while (Thread32Next(h_snapshot, &te32));
    }
    CloseHandle(h_snapshot);

    allocated_memory = VirtualAllocEx(h_process, NULL, 4096, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    if (!allocated_memory) {
        CloseHandle(h_process);
        error("Memory Allocation Failed", "Failed to allocate memory in the target process.");
    }

    h_thread = OpenThread(THREAD_ALL_ACCESS, FALSE, te32.th32ThreadID);
    if (!h_thread) {
        VirtualFreeEx(h_process, allocated_memory, NULL, MEM_RELEASE);
        CloseHandle(h_process);
        error("Thread Open Failed", "Failed to open a handle to the thread.");
    }

    SuspendThread(h_thread);
    ctx.ContextFlags = CONTEXT_FULL;
    GetThreadContext(h_thread, &ctx);

    buffer = VirtualAlloc(NULL, 65536, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    auto* ptr = static_cast<LPBYTE>(buffer);

    std::memcpy(buffer, SHELL_CODE, SHELL_CODE_SIZE);

    // Modify the shell code dynamically
    for (; *ptr != 0xC3; ++ptr) {
        if (*ptr == 0xB8 && *reinterpret_cast<PDWORD>(ptr + 1) == 0xCCCCCCCC) {
            *reinterpret_cast<PDWORD>(ptr + 1) = reinterpret_cast<DWORD>(LoadLibraryA);
        } else if (*ptr == 0x68 && *reinterpret_cast<PDWORD>(ptr + 1) == 0xCCCCCCCC) {
            *reinterpret_cast<PDWORD>(ptr + 1) = ctx.Eip;
        }
    }
    std::strcpy(reinterpret_cast<char*>(ptr), dll_path);

    if (!WriteProcessMemory(h_process, allocated_memory, buffer, SHELL_CODE_SIZE + std::strlen(dll_path) + 1, nullptr)) {
        VirtualFreeEx(h_process, allocated_memory, NULL, MEM_RELEASE);
        ResumeThread(h_thread);

        CloseHandle(h_thread);
        CloseHandle(h_process);

        VirtualFree(buffer, NULL, MEM_RELEASE);
        error("Memory Write Failed", "Failed to write the modified shell code to the target process.");
    }

    ctx.Eip = reinterpret_cast<DWORD>(allocated_memory);
    SetThreadContext(h_thread, &ctx);
    ResumeThread(h_thread);

    CloseHandle(h_thread);
    CloseHandle(h_process);
    VirtualFree(buffer, NULL, MEM_RELEASE);

    MessageBox(NULL, "Successfully injected", "Success", NULL);
    return 0;
}

} // namespace injector

#endif // DLL_INJECTOR_HPP